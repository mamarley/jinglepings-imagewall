import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.io.OutputStream;
import java.io.File;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;

public class ImageWall{
	public static String ipList=new String();
	public static String srcIp=null;
	
	public static Map<Process,Void> processMap=new HashMap<>();
	public static Boolean shuttingDown=false;
	
	public static void main(String[] args) throws IOException,InterruptedException{
		String imagePath=null;
		Integer xOffset=null;
		Integer yOffset=null;
		Integer numThreads=null;
		
		try{
			imagePath=args[0];
			xOffset=Integer.parseInt(args[1]);
			yOffset=Integer.parseInt(args[2]);
			numThreads=Integer.parseInt(args[3]);
			try{
				srcIp=args[4];
			}catch(ArrayIndexOutOfBoundsException e){
				// Om nom nom
			}
			
			if(imagePath==null||xOffset==null||xOffset<0||yOffset==null||yOffset<0||numThreads==null||numThreads<1){
				throw new IllegalArgumentException("");
			}
		}catch(Exception e){
			System.err.println("Invalid or incorrect arguments supplied.  Correct invocation is: \"java ImageWall <imagepath> <xoffset> <yoffset> <numthreads> [<srcip>]\".  Offsets must be non-negative integers.  Thread count must be positive.");
			System.exit(-1);
		}
		
		BufferedImage image=ImageIO.read(new File(imagePath));
		
		if(image.getType()!=BufferedImage.TYPE_3BYTE_BGR&&image.getType()!=BufferedImage.TYPE_4BYTE_ABGR&&image.getType()!=BufferedImage.TYPE_4BYTE_ABGR_PRE){
			System.err.println("The image must be 3-byte BGR or 4-byte ABGR image.  Grayscale and integer (A)BGR images are not supported.");
			System.exit(-1);
		}
		
		// Flip the image vertically and then rotate it 270 degrees clockwise.
		// This is because the image wall flips the image and rotates it 90 degrees clockwise,
		// so the transform here makes the image appear on the wall as it does on your screen.
		AffineTransform transform=AffineTransform.getScaleInstance(1,-1);
		transform.translate(0,-image.getHeight(null));
		transform.rotate(Math.toRadians(270),image.getWidth()/2,image.getHeight()/2);
		transform.translate((image.getWidth()-image.getHeight())/2,((image.getHeight()-image.getWidth())/2));
		AffineTransformOp op=new AffineTransformOp(transform,AffineTransformOp.TYPE_BILINEAR);
		image=op.filter(image,new BufferedImage(image.getHeight(),image.getWidth(),image.getType()));
		
		final byte[] pixels=((DataBufferByte)image.getRaster().getDataBuffer()).getData();
		final int width=image.getWidth();
		final int height=image.getHeight();
		final boolean hasAlphaChannel=(image.getAlphaRaster()!=null);
		byte pixelLength;
		
		if(hasAlphaChannel){
			pixelLength=4;
		}else{
			pixelLength=3;
		}
		
		for(int pixel=0,row=0,col=0;pixel<pixels.length;pixel+=pixelLength){
			byte pixelOffset=0;
			
			int alpha;
			if(hasAlphaChannel&&image.getType()!=BufferedImage.TYPE_4BYTE_ABGR_PRE){
				// Read the alpha value so it can be multiplied with the color channels later.
				// If the alpha value is 0, this pixel is entirely transparent so skip it.
				if(0==(alpha=((int)pixels[pixel+(pixelOffset++)]&0xff))){
					continue;
				}
			}else if(hasAlphaChannel){
				// Here the alpha value has already been multiplied with the color channels.
				// Set it to 255 to avoid doing so again below.
				// Still, if the alpha value is 0, skip the pixel entirely for the same reason as above.
				if(0==((int)pixels[pixel+(pixelOffset++)]&0xff)){
					continue;
				}
				alpha=255;
			}else{
				// If the image has no alpha channel, each pixel is fully opaque.
				alpha=255;
			}
			int blue=(alpha/255)*((int)pixels[pixel+(pixelOffset++)]&0xff);
			int green=(alpha/255)*((int)pixels[pixel+(pixelOffset++)]&0xff);
			int red=(alpha/255)*((int)pixels[pixel+(pixelOffset++)]&0xff);
			
			ipList=ipList+("2001:4c08:2028:"+(xOffset+row)+":"+(yOffset+col)+":"+Integer.toHexString(red)+":"+Integer.toHexString(green)+":"+Integer.toHexString(blue)+"\n");
			
			col++;
			if(col==width){
				col=0;
				row++;
			}
		}
		
		Runtime.getRuntime().addShutdownHook(new Thread(){
			public void run(){
				shuttingDown=true;
				synchronized(ImageWall.class){
					for(Process p:processMap.keySet()){
						p.destroy();
					}
				}
			}
		});
		
		Thread threads[]=new Thread[numThreads];
		for(int i=0;i<numThreads&&!shuttingDown;i++){
			threads[i]=new Thread(){
				public void run(){
					Process p=null;
					
					List<String> fpingArgList=new ArrayList<>();
					fpingArgList.add("fping");
					// Select the minimum possible delay.
					fpingArgList.add("-t1");
					// Select the minimum possible packet size to avoid transmitting pointless data.
					fpingArgList.add("--size=12");
					// Select the source IP, which is useful when SLAAC PE is in use.
					if(srcIp!=null){
						fpingArgList.add("--src="+srcIp);
					}
					String[] fpingArgs=new String[fpingArgList.size()];
					fpingArgs=fpingArgList.toArray(fpingArgs);
					
					while(!shuttingDown){
						synchronized(ImageWall.class){
							// Check the shuttingDown value again to make absolutely sure that no attempt
							// is made to spawn new processes after shutdown has begun.
							if(!shuttingDown){
								ProcessBuilder pb=new ProcessBuilder(fpingArgs);
								try{
									p=pb.start();
									processMap.put(p,null);
								}catch(Exception e){
									e.printStackTrace();
								}
							}else{
								continue;
							}
						}
						try{
							// Feed the previously-generated IP list to fping as STDIN.
							OutputStream stdInput=p.getOutputStream();
							stdInput.write(ipList.getBytes());
							stdInput.close();
							p.waitFor();
						}catch(Exception e){
							e.printStackTrace();
						}
						// Don't bother removing the process from the process list if a shutdown is occurring.
						// Since the program is about to terminate anyway, this is pointless and would cause a ConcurrentModificationException.
						if(!shuttingDown){
							synchronized(ImageWall.class){
								processMap.remove(p);
							}
						}
					}
				}
			};
			threads[i].start();
			// Sleep for 50 milliseconds between starting each thread to stagger which IP is being
			// pinged by which thread at what time.  This ensures that each pixel is pinged more often
			// instead of focusing the pings along a single "scanline."
			Thread.sleep(50);
		}
	}
}
