# jinglepings-imagewall

A Java program to display an image on the image wall at https://jinglepings.com/.

This program requires a Linux (or presumably other Unix-like) system with the "fping" program installed.  If these requirements are not met, the program will crash.

To compile it, just run <pre>javac ImageWall.java</pre>

To run it, execute <pre>`java ImageWall <filepath> <xoffset> <yoffset> <numthreads> [<srcip>]`</pre> where "filepath" is the path to an image file of a type supported by Java (such as PNG), "xoffset" and "yoffset" are the horizontal and vertical offsets from the top-left corner of the screen, and "numthreads" is the number of threads (and therefore fping processes) to spawn.  The "srcip" argument is optional and is used to specify the source IP option on fping, for example if your computer has multiple IPv6 addresses and only one of them is whitelisted by the image wall.

The program will load the image, rotate it clockwise 270° and vertically flip it (to compensate for silliness in how the image wall works; the image will actually appear on the wall the same as it appears on your PC), generate a list of IP addresses to be pinged to draw the image, and then launch the specified number of fping instances to ping those addresses.  On quit, the program will ensure all the fping processes are terminated.  Please do not forcefully terminate the program (signal 9), as this will leave the fping processes running.
